var express = require('express'),
	bodyParser = require('body-parser'),
	passport = require('passport'),
	auth = require('./services/auth'),
	static = require('node-static'),
	routes = {
		tag : require('./routes/tag-routes'),
		global : require('./routes/global-routes'),
		employer : require('./routes/employer-routes'),
		seeker : require('./routes/seeker-routes'),
		properties : require('./routes/properties-routes'),
		prospects : require('./routes/prospect-routes')
	};

/* App */

var app = express();

app.use(express.static('uploads'));

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(passport.initialize());

routes.tag(app);
routes.seeker(app);
routes.global(app);
routes.employer(app);
routes.properties(app);
routes.prospects(app);

var server = app.listen(3000, function () {
	console.log('listening at http://%s:%s', server.address().address, server.address().port);
});