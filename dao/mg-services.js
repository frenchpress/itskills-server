var mongoose = require('mongoose'),
	bcrypt = require('bcrypt-nodejs'),
	fs = require('fs'),
	url = "mongodb://localhost/",
	db = "itskills",
	_ = require('lodash'),
	properties = require('../services/properties-services.js');

mongoose.connect(url + db, function (err, res) {
	if (err) {
		console.log ('ERROR connecting ${err}');
	} else {
		console.log ('Succeeded connected');
	}
});

var Employer = require('../models/employer.js'),
	Seeker = require('../models/seeker.js'),
	Tag = require('../models/tag.js'),
	Job = require('../models/job.js')
	EmpProsp = require('../models/employerprospect.js'),
	SeekProsp = require('../models/seekerprospect.js');

/* Job */

function createJob(job, callback){
	var	newJob = new Job(job);
	newJob.views = 0;
	newJob.complete = false;
	newJob.visible = false;
	newJob.created = new Date();
	newJob.save(function(err, innerJob){
		if(callback){
			callback(!err, innerJob);
		}
	});
}

function getEmployerJobs(id, callback){
	Job.find({'employer': id}, function (err, jobs){
		var jobSorted = [];
		if(!jobs || !jobs.length){
			callback(!err, []);	
		}
		jobs.forEach(function(job){
			var innJob = job.toObject(),
				counter = 0;
			//Matches
			Seeker.count({
				favored : innJob._id,
				_id : { $in: innJob.favored }
			}, function(err, count){
				innJob.matches = count;
				counter++;
				if(counter === 3) jobSorted.push(innJob);
				if(jobSorted.length === jobs.length) callback(!err, jobSorted);
			});
			//Pending in
			Seeker.count({
				favored : innJob._id,
				_id : { $nin: innJob.favored }
			}, function(err, count){
				innJob.invitesIn = count;
				counter++;
				if(counter === 3) jobSorted.push(innJob);
				if(jobSorted.length === jobs.length) callback(!err, jobSorted);
			});
			//Pending out
			Seeker.count({
				favored: { $ne: innJob._id },
				_id : { $in: innJob.favored }
			}, function(err, count){
				innJob.invitesOut = count;
				counter++;
				if(counter === 3) jobSorted.push(innJob);
				if(jobSorted.length === jobs.length) callback(!err, jobSorted);
			});
		});
    });
}

function getSimpleEmployerJobs(id, callback){
	Job.find({'employer': id}, function (err, jobs){
		if(!jobs || !jobs.length){
			callback(!err, []);	
		}
		callback(!err, jobs);
    });
}

function inactivateEmployerJobs(email){
	Employer.findOne({ 'email': email }, function (err, employer){
        if(!err){
			Job.update({ employer: employer['_id'] }, { active: false }, {multi : true}, function (err, raw) {});
        }
    });
}

function getJobById(id, callback){
	Job.findById(id, function(err, job){
		if(!err && job)
			Employer.findById(job.employer, function(err, employer){
				callback(!err, job, employer);
			})
	});
}

function sortJob(employerId, jobId, rank, callback){
	Job.findOne({ '_id': jobId }, function (err, job) {
		if(err || !job || job.employer != employerId){
			callback(false, undefined);
		}
		job.rank = rank;
		job.save(function(err, job){
			callback(!err, job);
		});
	})
}

function updateJob(id, body, callback){
	Job.findOne({ '_id': id }, function (err, job) {
		if(err || !job){
			return false;
		}

		if(!job.active && body.active){
			console.log(new Date().getTime());
			job.started = new Date().getTime();
		}

		for(var ind in body){
			job[ind] = body[ind];
		}

		job.complete = true;
		var fields = ['title', 'description', 'location', 'salaryMin', 'salaryMax', 'roles', 'whyUs'];
		fields.forEach(function(field){
			if(!job[field]){
				job.complete = false;
				job.active = false;
			}
		});
		if(!job.tags || !job.tags.length || !job.xp || !job.xp.length || !job.types || !job.types.length){
			job.complete = false;
			job.active = false;
		}
		job.save(function(err, ans){
			callback(!err, ans);
		});
	})
}

function addJobFavored(jobId, seekerId, callback){
	Job.findOne({ '_id': jobId }, function (err, job){
        if(err || !job){
        	callback(false);
        } else{
        	job.favored.push(seekerId);
			job.save(function(err, ans){
				callback(!err);
			});
        }
    });
}

function addJobDiscarded(jobId, seekerId, callback){
	Job.findOne({ '_id': jobId }, function (err, job){
        if(err || !job){
        	callback(false);
        } else{
        	job.discarded.push(seekerId);
	        	job.save(function(err, ans){
				callback(!err);
			});
        }
    });
}

function removeJobFavored(jobId, seekerId, callback){
	Job.findOne({ '_id': jobId }, function (err, job){
        if(err || !job){
        	callback(false);
        } else{
        	var index = job.favored.indexOf(seekerId);
        	if(index !== -1){
        		job.favored.splice(index, 1);
        	}
			job.save(function(err, ans){
				callback(!err);
			});
        }
    });
}

function removeJobDiscarded(jobId, seekerId, callback){
	Job.findOne({ '_id': jobId }, function (err, job){
        if(err || !job){
        	callback(false);
        } else{
        	var index = job.discarded.indexOf(seekerId);
        	if(index !== -1){
        		job.discarded.splice(index, 1);
        	}
			job.save(function(err, ans){
				callback(!err);
			});
        }
    });
}

function addSeekerJobFavored(jobId, email, callback){
	Seeker.findOne({ 'email': email }, function (err, seeker){
        if(err || !seeker){
        	callback(false);
        } else{
        	if(seeker.favored.indexOf(jobId) === -1){
        		seeker.favored.push(jobId);	
        	}
			seeker.save(function(err, ans){
				callback(!err);
			});
        }
    });
}

function addSeekerJobDiscarded(jobId, email, callback){
	Seeker.findOne({ 'email': email }, function (err, seeker){
        if(err || !seeker){
        	callback(false);
        } else{
        		if(seeker.discarded.indexOf(jobId) === -1){
	        		seeker.discarded.push(jobId);
	        	}
	        	seeker.save(function(err, ans){
				callback(!err);
			});
        }
    });
}

function removeSeekerJobFavored(jobId, email, callback){
	Seeker.findOne({ 'email': email }, function (err, seeker){
        if(err || !seeker){
        	callback(false);
        } else{
        	var index = seeker.favored.indexOf(jobId);
        	if(index !== -1){
        		seeker.favored.splice(index, 1);
        	}
			seeker.save(function(err, ans){
				callback(!err);
			});
        }
    });
}

function removeSeekerJobDiscarded(jobId, email, callback){
	Seeker.findOne({ 'email': email }, function (err, seeker){
        if(err || !seeker){
        	callback(false);
        } else{
        	var index = seeker.discarded.indexOf(jobId);
        	if(index !== -1){
        		seeker.discarded.splice(index, 1);
        	}
			seeker.save(function(err, ans){
				callback(!err);
			});
        }
    });
}

function addAViewToJobs(ids){
	Job.find({ '_id': { $in: ids}}, function (err, jobs) {
		if(!err && jobs){
			jobs.forEach(function(j){
				j.views += 1;
				j.save(function(err, job){
					console.log(err);
					console.log(job);
				});
			});
		}
	})
}

/* Tags */

function addTag(label, callback){
	Tag.findOne({ 'label': label }, function (err, tag){
        if(!err && tag){
        	tag.count ++;
        	tag.save(function(err){
        		if(callback){
        			callback(!err);
        		}
			});
        } else if(!tag){
        	createTag(label, callback);
        } else {
        	if(callback){
        		callback(false);
        	}
        }
    });
}

function createTag(label, callback){
	var	newTag = new Tag({
		label : label,
		count : 1
	});

	newTag.save(function(err){
		if(callback){
			callback(!err);
		}
	});
}

function getTags(callback){
	Tag.find({}, function (err, tags){
		if(callback){
			callback(!err, tags);
		}
    });
}

/* Employer */

function getEmployer(email, callback){
	Employer.findOne({ 'email': email }, function (err, employer){
		if(employer) {
			getEmployerPlansDetails(employer, function(emp){
				callback(!err, emp);
			});
		} else {
			callback(!err, undefined);
		}
    });
}

function getEmployerPlansDetails(employer, callback){
	var plans = properties.properties.plans;
	getSimpleEmployerJobs(employer.id, function(success, jobs){
		var monthlyUsed = 0,
			planJoined = new Date(employer.planDate).getTime(),
			lastMonthlyTimestamp = new Date(employer.planDate).getTime(),
			now = new Date().getTime(),
			month = 2592000000;

		while(lastMonthlyTimestamp < now - month){
			lastMonthlyTimestamp += month;
		}

		jobs.forEach(function(job) {
			if(job.active && job.started && job.started > lastMonthlyTimestamp){
				monthlyUsed++;
			}
		});
		var emp = employer.toObject();
		emp.monthlyUsed = monthlyUsed;
		emp.nextReset = lastMonthlyTimestamp + month;

		plans.forEach(function(plan){
			if(plan.id == employer.plan){
				emp.monthlyLeft = plan.monthly - monthlyUsed;
			}
		});
		callback(emp);
	});
}

function getEmployerById(id, callback){
	Employer.findOne({ '_id': id }, function (err, employer){
        callback(!err, employer ? employer : undefined);
    });
}

function getEmployerId(email, callback){
	Employer.findOne({ 'email': email }, function (err, employer){
        callback(!err, employer ? employer._id : undefined);
    });
}

function registerEmployer(email, password, callback){
	Employer.findOne({ 'email': email }, function (err, employer){
        if(err || !employer){
        	createNewEmployer(email, password, callback);
        } else if(employer.active === false){
        	employer.active = true;
        	bcrypt.genSalt(5, function(err, salt) {
			    if (err){
			    	return;
			    }
			    bcrypt.hash(password, salt, null, function(err, hash) {
					employer.password = hash;
					employer.save(function(err){
						callback(!err ? 201 : 500);
					});
			    });
			});
        } else {
        	callback(409);
        }
    });
}

function createNewEmployer(email, password, callback){
	var	newEmployer = new Employer({
		email : email,
		active : true,
		plan : 4,
		planDate : new Date()
	});

	bcrypt.genSalt(5, function(err, salt) {
	    if (err){
	    	return;
	    }
	    bcrypt.hash(password, salt, null, function(err, hash) {
			newEmployer.password = hash;
			newEmployer.save(function(err){
				callback(!err ? 201 : 500);
			});
	    });
	});
}

function deleteEmployer(email, callback){

	Employer.findOne({ 'email': email }, function (err, employer){
		inactivateEmployerJobs(email);
		employer.active = false;
		employer.save(function(err, emp){
			callback(!err);
		});
    });
}

function deleteEmployerAvatars(email, callback){
	Employer.findOne({ 'email': email }, function (err, employer){
		employer.avatars.forEach(function(url){
			fs.unlinkSync(url);
		});
		employer.avatars = [];
		employer.save(function(err, emp){
			callback(!err);
		});
    });
}

function checkEmployerMailAvailability(email, callback){

	Employer.findOne({ 'email': email }, 'email', function (err, employer) {
		callback(!employer);
	})
}

function updateEmployer(email, body, callback){
	Employer.findOne({ 'email': email }, 'email', function (err, employer) {
		if(err || !employer){
			return false;
		}
		if(body.newEmail){
			body.email = body.newEmail;
		}
		for(var ind in body){
			if(ind === "social"){
				body[ind].forEach(function(x){
					if(x.url){
						if(employer.social){
							var found = false;
							employer.social.forEach(function(y){
								if(y.id === x.id){
									y.url = v.url;
									found = true;
								}
							});
							if(!found){
								employer.social.push({
									id : x.id,
									url : x.url
								});
							}
						} else {
							employer.social = [{
								id : x.id,
								url : x.url
							}];
						}
					}
				});
			} else {
				employer[ind] = body[ind];
			}
		}
		employer.save(function(err, emp){
			callback(!err, emp);
		});
	})
}

function addEmployerPicture(email, picture, callback){
	Employer.findOne({ 'email': email }, function (err, employer) {
		if(err || !employer){
			return false;
		}
		employer.avatar = picture.path;

		employer.save(function(err, emp){
			callback(err, emp);
		});
	})
}

/* Seeker */

function getSeeker(email, callback){
	Seeker.findOne({ 'email': email }, function (err, seeker){
        callback(!err, seeker ? seeker : undefined);
    });
}

function getSeekerById(id, callback){
	Seeker.findOne({ '_id': id }, function (err, seeker){
        callback(!err, seeker ? seeker : undefined);
    });
}

function getSeekersConnected(jobs, callback){
	var favoredSeekers = [],
		jobIds = _.pluck(jobs, '_id');
	jobs.forEach(function(job){
		favoredSeekers = favoredSeekers.concat(job.favored);
	});
	Seeker.find({
			active : true,
			$or: [ 
				{_id: {$in: favoredSeekers}},
				{favored: {$in: jobIds}}
			]
		}, function (err, seekers) {
		callback(!err, seekers);
	})
}

function registerSeeker(email, password, name, callback){
	Seeker.findOne({ 'email': email }, function (err, seeker){
        if(err || !seeker){
        	createNewSeeker(email, name, password, callback);
        } else if(seeker.active === false){
        	seeker.active = true;
        	bcrypt.genSalt(5, function(err, salt) {
			    if (err){
			    	return;
			    }
			    bcrypt.hash(password, salt, null, function(err, hash) {
					seeker.password = hash;
					seeker.save(function(err){
						callback(!err ? 201 : 500);
					});
			    });
			});
        } else {
        	callback(409);
        }
    });
}

function createNewSeeker(email, name, password, callback){
	var	newSeeker = new Seeker({
		email : email,
		name : name,
		active : true
	});

	bcrypt.genSalt(5, function(err, salt) {
	    if (err){
	    	return;
	    }
	    bcrypt.hash(password, salt, null, function(err, hash) {
			newSeeker.password = hash;
			newSeeker.save(function(err){
				callback(!err ? 201 : 500);
			});
	    });
	});
}

function updateSeeker(email, body, callback){
	Seeker.findOne({ 'email': email }, 'email', function (err, seeker) {
		if(err || !seeker){
			return false;
		}
		if(body.skills){
			body.skills.forEach(function(skill){
				addTag(skill);
			});
		}
		if(body.newEmail){
			body.email = body.newEmail;
		}
		for(var ind in body){
			seeker[ind] = body[ind];
		}
		seeker.save(function(err, seek){
			callback(!err, seek);
		});
	})
}

function deleteSeeker(email, callback){

    Seeker.findOne({ 'email': email }, function (err, seeker){
    	if(!err && seeker){
    		seeker.active = false;
			seeker.save(function(err, emp){
				callback(!err);
			});
    	} else {
    		callback(false);
    	}
		
    });
}

function checkSeekerMailAvailability(email, callback){

	Seeker.findOne({ 'email': email }, 'email', function (err, seeker) {
		callback(!seeker);
	})
}

function addSeekerCv(email, cv, callback){
	Seeker.findOne({ 'email': email }, function (err, seeker) {
		if(err || !seeker){
			return false;
		} else {
			seeker.cv = cv.path;

			seeker.save(function(err, emp){
				callback(err, emp);
			});
		}
	})
}

/* Match */

function getFirstSeekerBatch(job, emp, callback){
	var req = {
		status : 1, //Actively Looking
		active : true, 
		_id : { $nin: job.discarded },
		skills : { $in: job.tags },
		$or: [ 
			{location: job.location },
			{location: 1} //All New Zealand
		]
	};

	if(job.types.indexOf(5) !== -1){ //Remote
		req = {
			status : 1, //Actively Looking
			active : true, 
			_id : { $nin: job.discarded },
			skills : { $in: job.tags },
			$or: [ 
				{jobTypes: 5 }, //Remote
				{location: job.location },
				{location: 1} //All New Zealand
			]
		};
	}
	Seeker.find(req, function (err, seekers) {
		callback(!err, seekers);
	})
}

function getFirstJobBatch(seeker, callback){
	var req = {
		active : true, 
		visible : true,
		_id : { $nin: seeker.discarded },
		tags : { $in: seeker.skills },
		$or: [ 
			{location: seeker.location },
			{location: 1} //All New Zealand
		]
	};

	if(seeker.jobTypes.indexOf(5) !== -1){ //Remote
		req = {
			active : true, 
			visible : true,
			_id : { $nin: seeker.discarded },
			tags : { $in: seeker.skills },
			$or: [ 
				{types: 5 }, //Remote
				{location: seeker.location },
				{location: 1} //All New Zealand
			]
		};
	}
	Job.find(req, function (err, jobs) {
		callback(!err, jobs);
	})
}

function getJobsEmployers(jobs, callback){
	var employerIds = [];
	jobs.forEach(function(job){
		if(employerIds.indexOf(job.employer) === -1){
			employerIds.push(job.employer);
		}
	});
	Employer.find({'_id': {$in: employerIds}}, function (err, employers){
        jobs.forEach(function(job){
        	employers.forEach(function(emp){
        		if(job.employer == emp._id){
        			job.employer = emp;
        		}
        	});
        });
        callback(!err, jobs);
    });
}

/* Prospects */

function registerProspectEmp(email, callback){
	var	empProsp = new EmpProsp({
		email : email,
		date : new Date()
	});

	empProsp.save(function(err){
		callback(!err ? 201 : 500);
	});
}

function registerProspectSeeker(email, callback){
	var	seekProsp = new SeekProsp({
		email : email,
		date : new Date()
	});

	seekProsp.save(function(err){
		callback(!err ? 201 : 500);
	});
}

module.exports = {
	getTags : getTags,

	getEmployer : getEmployer,
	getEmployerById : getEmployerById,
	getEmployerId : getEmployerId,
	registerEmployer : registerEmployer,
	checkEmployerMailAvailability : checkEmployerMailAvailability,
	deleteEmployer : deleteEmployer,
	updateEmployer : updateEmployer,
	addEmployerPicture : addEmployerPicture,

	createJob : createJob,
	getEmployerJobs : getEmployerJobs,
	getJobById : getJobById,
	sortJob : sortJob,
	updateJob : updateJob,
	addJobFavored : addJobFavored,
	addJobDiscarded : addJobDiscarded,
	removeJobFavored : removeJobFavored,
	removeJobDiscarded : removeJobDiscarded,
	addSeekerJobFavored : addSeekerJobFavored,
	addSeekerJobDiscarded : addSeekerJobDiscarded,
	removeSeekerJobFavored : removeSeekerJobFavored,
	removeSeekerJobDiscarded : removeSeekerJobDiscarded,
	addAViewToJobs : addAViewToJobs,

	getSeeker : getSeeker,
	registerSeeker : registerSeeker,
	checkSeekerMailAvailability : checkSeekerMailAvailability,
	deleteSeeker : deleteSeeker,
	updateSeeker : updateSeeker,
	addSeekerCv : addSeekerCv,
	getSeekersConnected : getSeekersConnected,
	getSeekerById : getSeekerById,

	getFirstSeekerBatch : getFirstSeekerBatch,
	getFirstJobBatch : getFirstJobBatch,
	getJobsEmployers : getJobsEmployers,

	registerProspectSeeker : registerProspectSeeker,
	registerProspectEmp : registerProspectEmp
	
}