var mgServices = require('../dao/mg-services'),
	tools = require('./tools.js');

function get(ctx){
	mgServices.getTags(function(ans, tags){
		if(ans){
			ctx.res.status(200).send(tags);
		} else {
			ctx.res.sendStatus(500);
		}
	})
}

module.exports = {
	get : get
};