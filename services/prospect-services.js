var mgServices = require('../dao/mg-services');

function registerProspectEmp(ctx, email){
	mgServices.registerProspectEmp(email, function(succes){
		if(succes){
			ctx.res.sendStatus(201);
		} else {
			ctx.res.sendStatus(500);
		}
	})
}

function registerProspectSeeker(ctx, email){
	mgServices.registerProspectSeeker(email, function(succes){
		if(succes){
			ctx.res.sendStatus(201);
		} else {
			ctx.res.sendStatus(500);
		}
	})
}

module.exports = {
	registerProspectEmp : registerProspectEmp,
	registerProspectSeeker : registerProspectSeeker
};