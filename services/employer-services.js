var mgServices = require('../dao/mg-services'),
	tools = require('./tools.js');

function getEmployer(ctx, email){
	mgServices.getEmployer(email, function(succes, emp){
		if(succes){
			ctx.res.status(200).send(emp);
		} else {
			ctx.res.sendStatus(500);
		}
	})
}

function registerEmployer(ctx, email, password){
	var missing = tools.missingFields({email : email, password : password});
	if(missing){
		ctx.res.status(400).send(missing);
	} else if(!tools.validateEmail(email)){
		ctx.res.status(400).send({
			reason : "Invalid email",
			fields : ['email']
		});
	} else if (!tools.validatePassword(password)){
		ctx.res.status(400).send({
			reason : "Invalid password, must have between 6 and 30 letters",
			fields : ['password']
		});
	} else {
		mgServices.registerEmployer(email, password, function(status){
			ctx.res.sendStatus(status);
		});
	}
}

function update(ctx, mail, body){
	if(body.email === mail){
		delete body.password;
		mgServices.updateEmployer(mail, body, function(ans, emp){
			if(ans){
				delete emp.password;
				delete emp['_id'];
				ctx.res.status(200).send(emp);
			} else {
				ctx.res.sendStatus(204);
			}
		});
	} else {
		ctx.res.sendStatus(403);
	}
}

function addPicture(ctx, email, email2, picture){
	if(picture.size > 1000000){
		ctx.res.status(400).send({message : 'File size must be under 1MB.'});
	} else if(email === email2){
		mgServices.addEmployerPicture(email, picture, function(err, emp){
			ctx.res.sendStatus(!err && emp ? 200 : 500);
		});
	} else {
		ctx.res.sendStatus(403);
	}
}

function employerMailAvailable(ctx, mail){
	mgServices.checkEmployerMailAvailability(mail, function(ans){
		ctx.res.status(200).send({available : ans});
	});
}

function deleteEmployer(ctx, email){
	if(email === ctx.req.body.email)
		mgServices.deleteEmployer(email, function(success){
			ctx.res.sendStatus(success ? 204 : 404);
		});
	else
		ctx.res.sendStatus(403);
}

function createJob(ctx, email, job){
	mgServices.getEmployerId(email, function(ans, id){
		if(ans && id){
			job.employer = id;
			mgServices.createJob(job, function(ans, emp){
				if(ans && emp){
					ctx.res.status(201).send({_id : emp._id});
				} else {
					ctx.res.sendStatus(500);
				}
			});
		}
	});
}

function updateJob(ctx, id, body){
	mgServices.updateJob(id, body, function(ans, job){
		if(ans){
			ctx.res.status(200).send(job);
		} else {
			ctx.res.sendStatus(204);
		}
	});
}

function getJobs(ctx, email){
	mgServices.getEmployerId(email, function(ans, id){
		if(ans && id){
			mgServices.getEmployerJobs(id, function(ans2, jobs){
				if(ans2){
					ctx.res.status(200).send(jobs);
				} else {
					ctx.res.sendStatus(500);
				}
			});
		}
	});
}

function sortJobs(ctx, email, array){
	mgServices.getEmployerId(email, function(ans, id){
		if(ans && id){
			array.forEach(function(x) {
				mgServices.sortJob(id, x._id, x.rank, function(err, job) {});
			});
			ctx.res.sendStatus(200);
		} else {
			ctx.res.sendStatus(500);
		}
	});
}

function discard(ctx, jobId, seekerId){
	mgServices.addJobDiscarded(jobId, seekerId, function(ans){
		ctx.res.sendStatus(ans ? 200 : 500);
	});
}

function favor(ctx, jobId, seekerId){
	mgServices.addJobFavored(jobId, seekerId, function(ans){
		ctx.res.sendStatus(ans ? 200 : 500);
	});
}

function undiscard(ctx, jobId, seekerId){
	mgServices.removeJobDiscarded(jobId, seekerId, function(ans){
		ctx.res.sendStatus(ans ? 200 : 500);
	});
}

function unfavor(ctx, jobId, seekerId){
	mgServices.removeJobFavored(jobId, seekerId, function(ans){
		ctx.res.sendStatus(ans ? 200 : 500);
	});
}

function getSeeker(ctx, id){
	mgServices.getSeekerById(id, function(success, seeker){
		ctx.res.status(success ? 200 : 500).send(seeker);
	});
}

module.exports = {
	getEmployer : getEmployer,
	register : registerEmployer,
	delete : deleteEmployer,
	employerMailAvailable : employerMailAvailable,
	addPicture : addPicture,
	update : update,
	createJob : createJob,
	getJobs : getJobs,
	sortJobs : sortJobs,
	discard : discard,
	favor : favor,
	undiscard : undiscard,
	unfavor : unfavor,
	getSeeker : getSeeker,
	updateJob : updateJob
};