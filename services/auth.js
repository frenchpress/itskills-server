var passport = require('passport'),
    mongoose = require('mongoose'),
    LocalStrategy = require('passport-local').Strategy,
    Employer = require('../models/employer'),
    Seeker = require('../models/seeker');

passport.use('employer', new LocalStrategy({
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true
    }, function(req, email, password, done) {

        Employer.findOne({
            email: email
        }, function (err, employer) {

            if (err) { 
                return done(err);
            }

            if (!employer) {
                return done(null, false);
            }

            employer.verifyPassword(password, function(err, isMatch) {
                if (err) { 
                    return done(err);
                }

                if (!isMatch) { 
                    return done(null, false);
                }

                return done(null, employer);

            });
        });
    })
);

passport.use('seeker', new LocalStrategy({
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true
    }, function(req, email, password, done) {
        
        Seeker.findOne({
            email: email
        }, function (err, seeker) {

            if (err) { 
                return done(err);
            }

            if (!seeker) {
                return done(null, false);
            }

            seeker.verifyPassword(password, function(err, isMatch) {
                if (err) { 
                    return done(err);
                }

                if (!isMatch) { 
                    return done(null, false);
                }

                return done(null, seeker);

            });
        });
    })
);

module.exports = {
    employerAuthenticated : passport.authenticate('employer', { session : false }),
    seekerAuthenticated : passport.authenticate('seeker', { session : false })
};