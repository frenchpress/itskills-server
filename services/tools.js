module.exports = {
	validateEmail : function(email) {
	    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	    return re.test(email);
	},
	validatePassword : function(pwd){
		return pwd.length > 5 && pwd.length <= 30;
	},
	missingFields : function(obj){
		var missing = [];
		for(var ind in obj){
			if(obj.hasOwnProperty(ind) && !obj[ind]) {
				missing.push(ind);
			}
		}
		var ans = {
			reason : "Missing fields",
			fields : missing
		}
		return missing.length ? ans : false;
	}
};