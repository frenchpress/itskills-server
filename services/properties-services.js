"use strict";

var properties = {
	industries : [
		{id : 1, name : 'Accommodation, Hospitality'},
		{id : 2, name : 'Agriculture, Forestry'},
		{id : 3, name : 'Arts, Cultural & Recreation Services'},
		{id : 4, name : 'Business, Professional & Support Services'},
		{id : 5, name : 'Construction'},
		{id : 6, name : 'Education & Training'},
		{id : 7, name : 'Electricity, Gas, Water & Waste Services'},
		{id : 8, name : 'Financial & Insurance Services'},
		{id : 9, name : 'Health Care & Social Assistance'},
		{id : 10, name : 'Information Technology, Media & Telecommunications'},
		{id : 11, name : 'Manufacturing'},
		{id : 12, name : 'Mining'},
		{id : 13, name : 'Personal & Home Services'},
		{id : 14, name : 'Public Administration & Safety'},
		{id : 15, name : 'Rental, Hiring & Real Estate Services'},
		{id : 16, name : 'Retail Businesses'},
		{id : 17, name : 'Transport, Distribution & Warehousing'},
		{id : 18, name : 'Wholesale Businesses'},
		{id : 19, name : 'Other'}
	], 
	locations : [
		{id : 1, name : 'All New Zealand'},
		{id : 2, name : 'Auckland'},
		{id : 3, name : 'Northland'},
		{id : 4, name : 'Waikato'},
		{id : 5, name : 'Bay of Plenty'},
		{id : 6, name : 'Gisborne'},
		{id : 7, name : 'Hawkes Bay'},
		{id : 8, name : 'Taranaki'},
		{id : 9, name : 'Manawatu'},
		{id : 10, name : 'Wellington'},
		{id : 11, name : 'Tasman'},
		{id : 12, name : 'Marlborough'},
		{id : 13, name : 'Canterbury'},
		{id : 14, name : 'Otago'},
		{id : 15, name : 'West Coast'},
		{id : 16, name : 'Southland'}
	],
	companySizes : [
		{id : 1, name : 'Startup'},
		{id : 2, name : 'Tiny (<10 employees)'},
		{id : 3, name : 'Medium (10 - 50 employees)'},
		{id : 4, name : 'Large (50-200 employees)'},
		{id : 5, name : 'Huge (>200 employees)'},
	],
	degrees : [
		{id : 1, name : 'Not specified'},
		{id : 2, name : 'Undergraduate'},
		{id : 3, name : 'Post Graduate Degree'},
		{id : 4, name : 'Masters'},
		{id : 5, name : 'PhD'}
	],
	social : [
		{id : 1, name : 'linkedin'},
		{id : 2, name : 'twitter'},
		{id : 3, name : 'github'},
		{id : 4, name : 'facebook'},
		{id : 5, name : 'google-plus'},
		{id : 6, name : 'blog'},
		{id : 7, name : 'skype'}
	],
	status : [
		{id : 1, name : 'Actively Looking'},
		{id : 2, name : 'Secretly Looking'},
		{id : 3, name : 'Just Browsing'}
	],
	xp : [
		{id : 1, name : 'Junior (Less than 2 years)'},
		{id : 2, name : 'Intermediate (Between 2 and 5 years)'},
		{id : 3, name : 'Senior (More than 5 years)'}
	],
	jobType : [
		{id : 1, name : 'Full time'},
		{id : 2, name : 'Part time'},
		{id : 3, name : 'Contractor'},
		{id : 4, name : 'Intern'},
		{id : 5, name : 'Remote'}
	],
	salary : [
		{id : 1, name : '40k', value : '40000'},
		{id : 2, name : '60k', value : '60000'},
		{id : 3, name : '80k', value : '80000'},
		{id : 4, name : '100k', value : '100000'},
		{id : 5, name : '120k', value : '120000'},
		{id : 6, name : '150k', value : '150000'}
	],
	plans : [
		{id: 1, name: 'Basic', monthly: 2, pricing : 340},
		{id: 2, name: 'Intermediate', monthly: 5, pricing : 700},
		{id: 3, name: 'Advanced', monthly: 10, pricing : 1250},
		{id: 4, name: 'Elite', monthly: 25, pricing : 1900}
	]
};

module.exports = {
	getAll : function(ctx){
		ctx.res.status(200).send(properties);
	},
	properties : properties
}