var mgServices = require('../dao/mg-services'),
	_ = require('lodash');

//Matching seekers
function getSeekersForJob(ctx, jobId, employerMail){
	mgServices.getJobById(jobId, function(ans, job, employer){
		mgServices.getFirstSeekerBatch(job, employer, function(success, seekers) {
			calculateMatch(seekers, job, employer, function(seekers){
				seekers.sort(function(x, y){
					return x.rating < y.rating;
				});
				ctx.res.status(200).send(seekers);
			});
		});
	})
}

function calculateMatch(seekers,  job, employer, callback){
	var objects = [];
	if(seekers){
		seekers.forEach(function(seeker){
			var sk = seeker.toObject();
			delete sk.password;
			sk.rating = 50;
			calculateSeekerTags(job, sk);
			calculateSeekerXp(job, sk);
			calculateSeekerSize(employer, sk);
			calculateSeekerIndustry(employer, sk);
			if(sk.rating >= 60){
				objects.push(sk);
			}
		});
	}
	callback(objects);
}

function calculateSeekerTags(job, seeker){
	var matches = 0;
	job.tags.forEach(function(tag){
		matches += seeker.skills.indexOf(tag) === -1 ? 0 : 1;
	});
	if(matches >= 3 || matches === job.tags.length){
		seeker.rating += 20;
	} else {
		seeker.rating += 20 * (matches / 3);
	}
}

function calculateSeekerXp(job, seeker){
	if(job.xp && job.xp.length){
		seeker.rating += job.xp.indexOf(seeker.xp) === -1 ? 0 : 10;
	}
}

function calculateSeekerSize(employer, seeker){
	if(seeker.sizes && seeker.sizes.length){
		seeker.rating += seeker.sizes.indexOf(employer.size) === -1 ? 0 : 10;
	}
}

function calculateSeekerIndustry(employer, seeker){
	if(seeker.industries && seeker.industries.length){
		seeker.rating += seeker.industries.indexOf(employer.industry) === -1 ? 0 : 10;
	}
}

//Matching seekers with link

function getEmployerSeekers(ctx, email){
	mgServices.getEmployer(email, function(success, emp){
		if(success && emp){
			mgServices.getEmployerJobs(emp._id, function(succ, jobs){
				mgServices.getSeekersConnected(jobs, function(suc, seekers){
					if(suc){
						ctx.res.status(200).send({
							jobs : jobs,
							seekers : seekers
						});
					} else {
						ctx.res.sendStatus(500);
					}
				});
			});
		} else {
			ctx.res.sendStatus(500);
		}
	});
}

//Matching jobs
function getJobForSeekers(ctx, mail){
	mgServices.getSeeker(mail, function(succ, seeker){
		if(succ && seeker){
			var topJobs = [];
			mgServices.getFirstJobBatch(seeker, function(success, jobs) {
				if(success){
					jobs.forEach(function(job){
						var jb = job.toObject();
						jb.rating = 50;
						calculateJobTags(jb, seeker);
						calculateJobXp(jb, seeker);
						if(jb.rating >= 60){
							topJobs.push(jb);
						}
					});
					mgServices.getJobsEmployers(topJobs, function(success, jobs) {
						jobs.forEach(function(job){
							calculateJobSize(job, seeker);
							calculateJobIndustry(job, seeker);
							job.isFavored = seeker.favored.indexOf(job._id) !== -1;
						});
						
						mgServices.addAViewToJobs(_.pluck(jobs, '_id'));
						ctx.res.status(200).send(jobs);
					});
				}
			});
		}
	})
}

function calculateJobTags(job, seeker){
	var matches = 0;
	job.tags.forEach(function(tag){
		matches += seeker.skills.indexOf(tag) === -1 ? 0 : 1;
	});
	if(matches >= 3 || matches === job.tags.length){
		job.rating += 20;
	} else {
		job.rating += 20 * (matches / 3);
	}
}

function calculateJobXp(job, seeker){
	if(job.xp && job.xp.length){
		job.rating += job.xp.indexOf(seeker.xp) === -1 ? 0 : 10;
	}
}

function calculateJobSize(job, seeker){
	if(seeker.sizes && seeker.sizes.length){
		job.rating += seeker.sizes.indexOf(job.employer.size) === -1 ? 0 : 10;
	}
}

function calculateJobIndustry(job, seeker){
	if(seeker.industries && seeker.industries.length){
		job.rating += seeker.industries.indexOf(job.employer.industry) === -1 ? 0 : 10;
	}
}



module.exports = {
	getSeekersForJob : getSeekersForJob,
	getJobForSeekers : getJobForSeekers,
	getEmployerSeekers : getEmployerSeekers
};