var mgServices = require('../dao/mg-services'),
	tools = require('./tools.js');

function getSeeker(ctx, email){
	mgServices.getSeeker(email, function(succes, seeker){
		if(succes){
			ctx.res.status(200).send(seeker);
		} else {
			ctx.res.sendStatus(500);
		}
	})
}

function registerSeeker(ctx, email, password, name){
	var missing = tools.missingFields({email : email, password : password});
	if(missing){
		ctx.res.status(400).send(missing);
	} else if(!tools.validateEmail(email)){
		ctx.res.status(400).send({
			reason : "Invalid email",
			fields : ['email']
		});
	} else if (!tools.validatePassword(password)){
		ctx.res.status(400).send({
			reason : "Invalid password, must have between 6 and 30 letters",
			fields : ['password']
		});
	} else {
		mgServices.registerSeeker(email, password, name, function(status){
			ctx.res.sendStatus(status);
		})
	}
}

function update(ctx, mail, body){
	if(body.email === mail ) {
		delete body.password;
		mgServices.updateSeeker(mail, body, function(ans, seeker){
			if(ans) {
				delete seeker.password;
				delete seeker['_id'];
				ctx.res.status(200).send(seeker);
			} else {
				ctx.res.sendStatus(204);
			}	
		});
	} else {
		ctx.res.sendStatus(403);
	}
}

function seekerMailAvailable(ctx, mail){
	mgServices.checkSeekerMailAvailability(mail, function(ans){
		ctx.res.status(200).send({available : ans});
	})
}

function deleteSeeker(ctx, email){
	mgServices.deleteSeeker(email, function(success){
		ctx.res.sendStatus(success ? 204 : 404);
	});
}

function addCv(ctx, email, email2, cv){
	if(cv){
		if(cv.size > 2000000){
			ctx.res.status(400).send({message : 'File size must be under 2MB.'});
		} else if(email === email2){
			mgServices.addSeekerCv(email, cv, function(err, emp){
				ctx.res.status(!err && emp ? 200 : 500).send(emp);	
			});
		} else {
			ctx.res.sendStatus(403);
		}
	} else {
		ctx.res.status(400).send({message : 'No CV enclosed in the request.'});
	}
	
}

function discard(ctx, jobId, email){
	mgServices.addSeekerJobDiscarded(jobId, email, function(ans){
		ctx.res.sendStatus(ans ? 200 : 500);
	});
}

function favor(ctx, jobId, email){
	mgServices.addSeekerJobFavored(jobId, email, function(ans){
		ctx.res.sendStatus(ans ? 200 : 500);
	});
}

function undiscard(ctx, jobId, email){
	mgServices.removeSeekerJobDiscarded(jobId, email, function(ans){
		ctx.res.sendStatus(ans ? 200 : 500);
	});
}

function unfavor(ctx, jobId, email){
	mgServices.removeSeekerJobFavored(jobId, email, function(ans){
		ctx.res.sendStatus(ans ? 200 : 500);
	});
}

function getEmployer(ctx, id){
	mgServices.getEmployerById(id, function(succes, emp){
		ctx.res.status(succes ? 200 : 500).send(emp ? emp : {});
	});
}

module.exports = {
	getSeeker : getSeeker,
	register : registerSeeker,
	delete : deleteSeeker,
	seekerMailAvailable : seekerMailAvailable,
	update : update,
	addCv : addCv,
	discard : discard,
	favor : favor,
	undiscard : undiscard,
	unfavor : unfavor,
	getEmployer : getEmployer
};