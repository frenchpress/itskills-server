var mgServices = require('../dao/mg-services');

function getJobById(ctx, id){
	mgServices.getJobById(id, function(ans, job, emp){
		if(ans && job && emp){
			var jobInfo = job.toObject();
			var employerDetails = emp.toObject();
			delete jobInfo._id;
			delete jobInfo.employer;
			delete jobInfo.__v;
			delete employerDetails._id;
			delete employerDetails.password;

			jobInfo.emp = employerDetails; 
			ctx.res.status(200).send(jobInfo);
		}
	})
}

module.exports = {
	getJobById : getJobById,
};