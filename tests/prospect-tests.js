var should = require('chai').should(),
	expect = require('chai').expect,
	supertest = require('supertest'),
	api = supertest('http://localhost:3000');

describe('Prospects Creation', function(){

	it('Should create a employer prospect', function(done){
		api.post('/prospect/employer')
			.set('Accept', 'application/json')
			.send({
				email : 'test@test.com'
			})
			.expect(201, done);
	});

	it('Should create a seeker prospect', function(done){
		api.post('/prospect/seeker')
			.set('Accept', 'application/json')
			.send({
				email : 'test@test.com'
			})
			.expect(201, done);
	});

});