var should = require('chai').should(),
	expect = require('chai').expect,
	supertest = require('supertest'),
	api = supertest('http://localhost:3000');

describe('Employer Creation', function(){

	it('Should create an employer', function(done){
		api.post('/employer')
			.set('Accept', 'application/json')
			.send({
				name : 'Test Company',
				email : 'test@test.com',
				password : 'testtest'
			})
			.expect(201, done);
	});

	it('Should create another employer', function(done){
		api.post('/employer')
			.set('Accept', 'application/json')
			.send({
				name : 'Test Another Company',
				email : 'first@test.com',
				password : 'testtest'
			})
			.expect(201, done);
	});

	it('Shouldn\'t create an employer without a valid email', function(done){
		api.post('/employer')
			.set('Accept', 'application/json')
			.send({
				name : 'Test Company',
				email : 'test',
				password : 'testtest'
			})
			.expect(400, done);
	});

	it('Shouldn\'t create an employer without a tiny password', function(done){
		api.post('/employer')
			.set('Accept', 'application/json')
			.send({
				name : 'Test Company',
				email : 'test@test.com',
				password : 'ted'
			})
			.expect(400, done);
	});

	it('Shouldn\'t create a second employer with the same mail', function(done){
		api.post('/employer')
			.set('Accept', 'application/json')
			.send({
				name : 'Test Company',
				email : 'test@test.com',
				password : 'testtest'
			})
			.expect(409, done);
	});

	/*it('Shouldn\'t create an employer without name', function(done){
		api.post('/employer')
			.set('Accept', 'application/json')
			.send({
				email : 'test@test.com',
				password : 'testtest'
			})
			.expect(400, done);
	});*/
});

describe('Employer Update', function(){
	it('Shouldn\'t find a employer to update', function(done){
		api.put('/employer/fewfewfewgew')
			.set('Accept', 'application/json')
			.send({
				email : 'test@test.com',
				password : 'testtest'
			})
			.expect(403, done);
	});

	it('Shouldn\'t be allowed to update another employer', function(done){
		api.put('/employer/first@test.com')
			.set('Accept', 'application/json')
			.send({
				email : 'test@test.com',
				password : 'testtest'
			})
			.expect(403, done);
	});

	it('Should update the created employer', function(done){
		api.put('/employer/test@test.com')
			.set('Accept', 'application/json')
			.send({
				email : 'test@test.com',
				password : 'testtest',
				address: "address",
				companyName: "companyName",
				phone: "0541651651",
				website: "website",
				size : 1,
				industry : 5,
				description : 'description',
				social : [{
					id : 1,
					url : 'fewfewfew.ffwefew'
				}, {
					id : 3,
					url : 'feewfew.ffwefew'
				}]
			})
			.expect(200)
			.end(function(err, res){
				expect(res.body.email).to.be.equal('test@test.com');
				expect(res.body.address).to.be.equal('address');
				expect(res.body.companyName).to.be.equal('companyName');
				expect(res.body.phone).to.be.equal('0541651651');
				expect(res.body.website).to.be.equal('website');
				expect(res.body.password).to.be.undefined;
				expect(res.body.size).to.be.equal(1);
				expect(res.body.industry).to.be.equal(5);
				expect(res.body.description).to.be.equal('description');
				done();
			});
	});
});

describe('Employer Update', function(){
	it('Should upload an image', function(done) {
       api.post('/employer/test@test.com/picture')
		.field('email', 'test@test.com')
		.field('password', 'testtest')
		.attach('avatar', '../assets/test.png')
		.expect(200, done)
    });
});

describe('Employer mail availability', function(){

	it('Should tell me that a random valid mail is available', function(done){
		api.get('/employer/mail/fewfewfew@aaaaa.com/available')
			.set('Accept', 'application/json')
			.send()
			.expect(200)
			.end(function(err, res){
				expect(res.body.available).to.be.equal(true);
				done();
			});
	});

	it('Shouldn\'t tell me that a used mail is available', function(done){
		api.get('/employer/mail/test@test.com/available')
			.set('Accept', 'application/json')
			.send()
			.expect(200)
			.end(function(err, res){
				expect(res.body.available).to.be.equal(false);
				done();
			});
	});
});

describe('Employer Auth', function(){

	it('Should let me delete the employer without valid credential', function(done){
		api.delete('/employer/test@test.com')
			.set('Accept', 'application/json')
			.send({
				email : 'haha@haha.com',
				password : 'TTTTTTTTTT'
			})
			.expect(401, done);
	});
});

var jobIdArray = [];
describe('Employer Jobs', function(){

	var job = {
		title : 'title1',
		tags : ["javascript", "java"],
		description : 'description',
		location : 2,
		salaryMin : 1,
		salaryMax : 2,
		xp : [1, 2],
		availability : new Date(), 
		types : [5, 4],
		mail : 'mail',
		active : true,
		industry : 5,
		rank : 1
	}, job2 = {
		title : 'title2',
		tags : ['javascript', 'ruby'],
		description : 'description',
		location : 10,
		salaryMin : 1,
		salaryMax : 2,
		xp : [2],
		availability : new Date(), 
		types : [3, 4],
		mail : 'mail',
		active : true,
		industry : 10,
		rank : 2
	}, job3 = {
		title : 'title3',
		tags : ['java', 'python'],
		description : 'description',
		location : 1,
		salaryMin : 1,
		salaryMax : 2,
		xp : [3],
		availability : new Date(), 
		types : [1, 3],
		mail : 'mail',
		active : true,
		industry : '13',
		rank : 3
	};

	it('Should create a complete job', function(done){
		api.post('/employer/jobs')
			.set('Accept', 'application/json')
			.send({
				email : 'test@test.com',
				password : 'testtest',
				job : job
			})
			.expect(201, done)
	});

	it('Should create a second job', function(done){
		api.post('/employer/jobs')
			.set('Accept', 'application/json')
			.send({
				email : 'test@test.com',
				password : 'testtest',
				job : job2
			})
			.expect(201, done)
	});

	it('Should create a third job', function(done){
		api.post('/employer/jobs')
			.set('Accept', 'application/json')
			.send({
				email : 'test@test.com',
				password : 'testtest',
				job : job3
			})
			.expect(201, done)
	});

	it('Should find the previous job', function(done){
		api.get('/employer/jobs?email=test@test.com&password=testtest')
			.set('Accept', 'application/json')
			.send()
			.expect(200)
			.end(function(err, res){
				var found = false;
				res.body.forEach(function(innerJob){
					jobIdArray.push(innerJob._id);
					if(innerJob.title === job.title && innerJob.active){
						found = true;
						expect(innerJob.views).to.be.equal(0);
						expect(innerJob.tags.length).to.be.equal(2);
						expect(innerJob.tags[0]).to.be.equal(job.tags[0]);
						expect(innerJob.tags[1]).to.be.equal(job.tags[1]);
						expect(innerJob.description).to.be.equal(job.description);
						expect(innerJob.location).to.be.equal(job.location);
						expect(innerJob.salaryMin).to.be.equal(job.salaryMin);
						expect(innerJob.salaryMax).to.be.equal(job.salaryMax);
						expect(innerJob.xp.length).to.be.equal(2);
						expect(innerJob.xp[0]).to.be.equal(job.xp[0]);
						expect(innerJob.xp[1]).to.be.equal(job.xp[1]);
						expect(innerJob.types.length).to.be.equal(2);
						expect(innerJob.types[0]).to.be.equal(job.types[0]);
						expect(innerJob.types[1]).to.be.equal(job.types[1]);
						expect(innerJob.mail).to.be.equal(job.mail);
						expect(innerJob.active).to.be.equal(job.active);
						expect(innerJob.active).to.be.equal(true);
					}
				});
				expect(found).to.be.equal(true);
				done();
			});
	});

	it('Should shuffle the ranks', function(done){
		var array = [];
		jobIdArray.forEach(function(x, index){
			array.push({
				_id : x,
				rank : ((index + 1) % 3) + 1
			});
		});
		api.put('/employer/jobs/sort')
			.set('Accept', 'application/json')
			.send({
				email : 'test@test.com',
				password : 'testtest',
				array : array
			})
			.expect(200, done)
	});

	it('Should check the modified ranks', function(done){
		var checkRank = [2, 3, 1];
		api.get('/employer/jobs?email=test@test.com&password=testtest')
			.set('Accept', 'application/json')
			.send()
			.expect(200)
			.end(function(err, res){
				res.body.forEach(function(innerJob, index){
					expect(innerJob.rank).to.be.equal(checkRank[index]);
				});
				done();
			});
	});

	it('Should update the previous job', function(){
		api.put('/employer/jobs/'+ jobIdArray[1])
			.set('Accept', 'application/json')
			.send({
				email : "test@test.com",
				password : "testtest",
				title : 'AngularJs Dev',
				tags : ['javascript', 'ruby', 'css'],
				description : 'Best angularJs dev, with special skills',
				roles : 'roles',
				whyUs : 'why us',
				location : 7,
				salaryMin : 2,
				salaryMax : 3,
				xp : [3],
				types : [3, 4]
			})
			.expect(200)
			.end(function(err, res){
				expect(res.body.title).to.be.equal('AngularJs Dev');
				expect(res.body.description).to.be.equal('Best angularJs dev, with special skills');
				expect(res.body.roles).to.be.equal('roles');
				expect(res.body.whyUs).to.be.equal('why us');
				expect(res.body.location).to.be.equal(7);
				expect(res.body.tags[0]).to.be.equal('javascript');
				expect(res.body.tags[1]).to.be.equal('ruby');
				expect(res.body.tags[2]).to.be.equal('css');
				expect(res.body.salaryMin).to.be.equal(2);
				expect(res.body.salaryMax).to.be.equal(3);
				expect(res.body.types[0]).to.be.equal(3);
				expect(res.body.types[1]).to.be.equal(4);
			})
	});
	
});

describe('Employer Favor', function(){
	it('Should favor a seeker', function(done){
		api.post('/employer/jobs/'+jobIdArray[0]+'/favor')
			.set('Accept', 'application/json')
			.send({
				email : 'test@test.com',
				password : 'testtest',
				seekerId : 44
			})
			.expect(200, done)
	});

	it('Should unfavor a seeker', function(done){
		api.post('/employer/jobs/'+jobIdArray[0]+'/unfavor')
			.set('Accept', 'application/json')
			.send({
				email : 'test@test.com',
				password : 'testtest',
				seekerId : 44
			})
			.expect(200, done)
	});
});

describe('Employer Discard', function(){
	it('Should discard a seeker', function(done){
		api.post('/employer/jobs/'+jobIdArray[0]+'/discard')
			.set('Accept', 'application/json')
			.send({
				email : 'test@test.com',
				password : 'testtest',
				seekerId : 49
			})
			.expect(200, done)
	});

	it('Should undiscard a seeker', function(done){
		api.post('/employer/jobs/'+jobIdArray[0]+'/undiscard')
			.set('Accept', 'application/json')
			.send({
				email : 'test@test.com',
				password : 'testtest',
				seekerId : 49
			})
			.expect(200, done)
	});
});
/*
describe('Employer Deletion', function(){
	it('Should\'nt delete a non existing employer', function(done){
		api.delete('/employer/tefewfewst@tefwfwst.com')
			.set('Accept', 'application/json')
			.send({
				email : 'test@test.com',
				password : 'testtest'
			})
			.expect(403, done);
	});

	it('Should\'nt delete the employer with different mail', function(done){
		api.delete('/employer/first@test.com')
			.set('Accept', 'application/json')
			.send({
				email : 'test@test.com',
				password : 'testtest'
			})
			.expect(403, done);
	});

	it('Should delete the employer', function(done){
		api.delete('/employer/test@test.com')
			.set('Accept', 'application/json')
			.send({
				email : 'test@test.com',
				password : 'testtest'
			})
			.expect(204, done);
	});

	it('Should delete the employer', function(done){
		api.delete('/employer/first@test.com')
			.set('Accept', 'application/json')
			.send({
				email : 'first@test.com',
				password : 'testtest'
			})
			.expect(204, done);
	});
});
describe('Employer Jobs deactivation', function(){
	it('Should find only inactivated jobs', function(done){
		api.get('/employer/jobs?email=test@test.com&password=testtest')
			.set('Accept', 'application/json')
			.send()
			.expect(200)
			.end(function(err, res){
				res.body.forEach(function(inJob){
					expect(inJob.active).to.be.equal(false);
				});
				done();
			});
	});
});
*/