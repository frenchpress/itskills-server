var should = require('chai').should(),
	expect = require('chai').expect,
	supertest = require('supertest'),
	api = supertest('http://localhost:3000'),
  	fs = require("fs"),
	empFile = fs.readFileSync("employers-data.json"),
	seekerFile = fs.readFileSync("seekers-data.json"),
	jobsFile = fs.readFileSync("jobs-data.json"),
 	empJson = JSON.parse(empFile),
 	seekerJson = JSON.parse(seekerFile),
 	jobsJson = JSON.parse(jobsFile);

describe('Employer Creation', function(){
	var counter = 0;
	// Counter test 
		
		it('Should create an employer', function(done){
			for(var i = 0; i < empJson.length; i++){
			api.post('/employer')
				.set('Accept', 'application/json')
				.send({
					name : empJson[i].name,
					email : empJson[i].email,
					password : empJson[i].password
				})
				.expect(201)
				.end(function(err, res){
					counter++;
					if(counter === empJson.length - 1)
						done();
			})
		}
	});	
});


describe('Employer Update', function(){
	var counter = 0;
	it('Should update the created employer', function(done){
		for (var i = 0; i < empJson.length; i++) {

		api.put('/employer/'+ empJson[i].email)
			.set('Accept', 'application/json')
			.send({
				email : empJson[i].email,
				password : empJson[i].password,
				address: empJson[i].address,
				companyName: empJson[i].companyName,
				phone: empJson[i].phone,
				website: empJson[i].website,
				size : empJson[i].size,
				industry : empJson[i].industry,
				description : empJson[i].description,
				social : empJson[i].social
			})
			.expect(200)
			.end(function(err, res){
				counter++;
				if(counter === empJson.length - 1)
					done();
			});
		};
	});
});

describe('Employer Jobs', function(){
	var counter = 0;
	var jobcounter = 0;
	it('Should create a complete job', function(done){
		for (var i = 0; i < empJson.length; i++) {
			api.post('/employer/jobs')
			.set('Accept', 'application/json')
			.send({
				email : empJson[i].email,
				password : empJson[i].password,
				job : jobsJson[jobcounter]
			})
			.expect(201)
			.end(function(err, res){
				counter++;
				if(counter === empJson.length - 1)
					done();
			});
			jobcounter++;

			api.post('/employer/jobs')
			.set('Accept', 'application/json')
			.send({
				email : empJson[i].email,
				password : empJson[i].password,
				job : jobsJson[jobcounter]
			})
			.expect(201)
			.end(function(err, res){
				counter++;
				if(counter === empJson.length - 1)
					done();
			});
			jobcounter++;
		};
		
	});
});

describe('Seekers Creation', function(){
	var counter = 0;
		it('Should create an seeker', function(done){
			for(var i = 0; i < seekerJson.length; i++){
			api.post('/seeker')
				.set('Accept', 'application/json')
				.send({
					email : seekerJson[i].email,
					password : seekerJson[i].password
				})
				.expect(201)
				.end(function(err, res){
					counter++;
					if(counter === seekerJson.length - 1)
						done();
			})
		}
	});	
});


describe('Seeker Update', function(){
	var counter = 0;
	it('Should update the created seeker', function(done){
		for (var i = 0; i < seekerJson.length; i++) {

		api.put('/seeker/'+ seekerJson[i].email)
			.set('Accept', 'application/json')
			.send({
				name : seekerJson[i].name,
				email : seekerJson[i].email,
				password : seekerJson[i].password,
				location: seekerJson[i].location,
				xp: seekerJson[i].xp,
				phone: seekerJson[i].phone,
				companySizes: seekerJson[i].companySizes,
				education : seekerJson[i].education,
				jobTypes : seekerJson[i].jobTypes,
				status : seekerJson[i].status,
				social : seekerJson[i].social,
				description : seekerJson[i].description,
				salary : seekerJson[i].salary,
				skills : seekerJson[i].skills,
				industries : seekerJson[i].industries,
				
			})
			.expect(200)
			.end(function(err, res){
				counter++;
				if(counter === seekerJson.length - 1)
					done();
			});
		};
	});
});

