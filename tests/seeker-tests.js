var should = require('chai').should(),
	expect = require('chai').expect,
	supertest = require('supertest'),
	api = supertest('http://localhost:3000');

describe('Seeker Creation', function(){
	it('Should create a seeker', function(done){
		api.post('/seeker')
			.set('Accept', 'application/json')
			.send({
				email : 'test@test.com',
				password : 'testtest'
			})
			.expect(201, done);
	});

	it('Should create test2', function(done){
		api.post('/seeker')
			.set('Accept', 'application/json')
			.send({
				email : 'test2@test.com',
				password : 'testtest'
			})
			.expect(201, done);
	});

	it('Should create test3', function(done){
		api.post('/seeker')
			.set('Accept', 'application/json')
			.send({
				email : 'test3@test.com',
				password : 'testtest'
			})
			.expect(201, done);
	});

	it('Shouldn\'t create a seeker without a valid email', function(done){
		api.post('/seeker')
			.set('Accept', 'application/json')
			.send({
				email : 'test',
				password : 'testtest'
			})
			.expect(400, done);
	});

	it('Shouldn\'t create a seeker without a tiny password', function(done){
		api.post('/seeker')
			.set('Accept', 'application/json')
			.send({
				email : 'test@test.com',
				password : 'ted'
			})
			.expect(400, done);
	});

	it('Shouldn\'t create a second seeker with the same mail', function(done){
		api.post('/seeker')
			.set('Accept', 'application/json')
			.send({
				email : 'test@test.com',
				password : 'testtest'
			})
			.expect(409, done);
	});
});

describe('Seeker Update', function(){
	it('Shouldn\'t find a seeker to update', function(done){
		api.put('/seeker/fewfewfewgew')
			.set('Accept', 'application/json')
			.send({
				email : 'test@test.com',
				password : 'testtest'
			})
			.expect(403, done);
	});

	it('Shouldn\'t be allowed to update another seeker', function(done){
		api.put('/seeker/first@test.com')
			.set('Accept', 'application/json')
			.send({
				email : 'test@test.com',
				password : 'testtest'
			})
			.expect(403, done);
	});

	it('Should upload an image', function(done) {
       api.post('/seeker/test@test.com/cv')
		.field('email', 'test@test.com')
		.field('password', 'testtest')
		.attach('cv', '../assets/test.png')
		.expect(200, done)
    });

	it('Should update the created seeker', function(done){
		api.put('/seeker/test@test.com')
			.set('Accept', 'application/json')
			.send({
				email : 'test@test.com',
				password : 'testtest',
				location: 10,
				name: "John Doe",
				phone: "0541651651",
				xp: 2,
				education : 4,
				jobTypes : [2, 5],
				status : 1,
				companySizes: [1, 2],
				industries: [2, 12],
				skills: ['javascript', 'ruby'],
				salary: 2,
				social: [{id : 1, url : 'url1'}, {id : 3, url : 'url2'}],
				description: 'description'
			})
			.expect(200)
			.end(function(err, res){
				expect(res.body.email).to.be.equal('test@test.com');
				expect(res.body.password).to.be.undefined;
				expect(res.body.location).to.be.equal(10);
				expect(res.body.name).to.be.equal('John Doe');
				expect(res.body.phone).to.be.equal('0541651651');
				expect(res.body.xp).to.be.equal(2);
				expect(res.body.education).to.be.equal(4);
				expect(res.body.description).to.be.equal('description');
				expect(res.body.status).to.be.equal(1);

				expect(res.body.jobTypes.length).to.be.equal(2);
				expect(res.body.jobTypes[0]).to.be.equal(2);
				expect(res.body.jobTypes[1]).to.be.equal(5);

				expect(res.body.companySizes.length).to.be.equal(2);
				expect(res.body.companySizes[0]).to.be.equal(1);
				expect(res.body.companySizes[1]).to.be.equal(2);

				expect(res.body.industries.length).to.be.equal(2);
				expect(res.body.industries[0]).to.be.equal(2);
				expect(res.body.industries[1]).to.be.equal(12);

				expect(res.body.skills.length).to.be.equal(2);
				expect(res.body.skills[0]).to.be.equal('javascript');
				expect(res.body.skills[1]).to.be.equal('ruby');

				expect(res.body.salary).to.be.equal(2);

				expect(res.body.social.length).to.be.equal(2);
				expect(res.body.social[0].id).to.be.equal(1);
				expect(res.body.social[0].url).to.be.equal('url1');
				expect(res.body.social[1].id).to.be.equal(3);
				expect(res.body.social[1].url).to.be.equal('url2');
				
				done();
			});
	});

	it('Should update the second seeker', function(done){
		api.put('/seeker/test2@test.com')
			.set('Accept', 'application/json')
			.send({
				email : 'test2@test.com',
				password : 'testtest',
				location: 2,
				name: "John Doe",
				phone: "0541651651",
				xp: 2,
				education : 4,
				jobTypes : [3, 5],
				status : 1,
				companySizes: [1, 2, 3],
				industries: [3, 8],
				skills: ['javascript', 'ruby'],
				salary: 3,
				social: [{id : 1, url : 'url1'}, {id : 3, url : 'url2'}],
				description: 'description'
			})
			.expect(200, done);
	});

	it('Should update the third seeker', function(done){
		api.put('/seeker/test3@test.com')
			.set('Accept', 'application/json')
			.send({
				email : 'test3@test.com',
				password : 'testtest',
				location: 10,
				name: "John Doe",
				phone: "0541651651",
				xp: 2,
				education : 4,
				jobTypes : [1, 2, 3],
				status : 1,
				companySizes: [1, 2],
				industries: [4, 7],
				skills: ['javascript', 'ruby', 'java'],
				salary: 3,
				social: [{id : 1, url : 'url1'}, {id : 3, url : 'url2'}],
				description: 'description'
			})
			.expect(200, done);
	});
});

describe('Tags', function(){
	var records = [];
	it('Should get tags and find previously updated tags', function(done){
		api.get('/tags')
			.set('Accept', 'application/json')
			.send()
			.expect(200)
			.end(function(err, res){
				if(res.body){
					res.body.forEach(function(tag){
						if(tag.label === "javascript" || tag.label === "ruby"){
							records.push(tag);
						}
					});
				}
				expect(records.length).to.be.equal(2);
				done();
			});
	});

	it('Should update the created seeker and take into account the tags', function(done){
		api.put('/seeker/test@test.com')
			.set('Accept', 'application/json')
			.send({
				email : 'test@test.com',
				password : 'testtest',
				skills: ['javascript', 'ruby']
			})
			.expect(200)
			.end(function(err, res){
				expect(res.body.email).to.be.equal('test@test.com');
				expect(res.body.password).to.be.undefined;
				done();
			});
	});

	it('Should get tags and find previously updated tags', function(done){
		api.get('/tags')
			.set('Accept', 'application/json')
			.send()
			.expect(200)
			.end(function(err, res){
				var found = 0;
				if(res.body){
					records.forEach(function(rec){
						res.body.forEach(function(tag){
							if(rec.label === tag.label){
								found += (+rec.count + 1 === +tag.count) ? 1 : 0;
							}
						});
					});
				}
				expect(found).to.be.equal(2);
				done();
			});
	});
});

describe('Seeker mail availability', function(){
	it('Should tell me that a random valid mail is available', function(done){
		api.get('/seeker/mail/fewfewfew@aaaaa.com/available')
			.set('Accept', 'application/json')
			.send()
			.expect(200)
			.end(function(err, res){
				expect(res.body.available).to.be.equal(true);
				done();
			});
	});

	it('Shouldn\'t tell me that a used mail is available', function(done){
		api.get('/seeker/mail/test@test.com/available')
			.set('Accept', 'application/json')
			.send()
			.expect(200)
			.end(function(err, res){
				expect(res.body.available).to.be.equal(false);
				done();
			});
	});
});

describe('Seeker Auth', function(){
	it('Should let me delete the seeker without valid credential', function(done){
		api.delete('/seeker/test@test.com')
			.set('Accept', 'application/json')
			.send({
				email : 'haha@haha.com',
				password : 'TTTTTTTTTT'
			})
			.expect(401, done);
	});
});
/*

describe('Seeker Deletion', function(){

	it('Should\'nt delete a non existing seeker', function(done){
		api.delete('/seeker/tefewfewst@tefwfwst.com')
			.set('Accept', 'application/json')
			.send({
				email : 'test@test.com',
				password : 'testtest'
			})
			.expect(404, done);
	});

	it('Should delete the seeker', function(done){
		api.delete('/seeker/test@test.com')
			.set('Accept', 'application/json')
			.send({
				email : 'test@test.com',
				password : 'testtest'
			})
			.expect(204, done);
	});

});
*/