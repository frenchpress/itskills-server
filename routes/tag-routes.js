var service = require('../services/tag-services');

module.exports = function(app){

	//Fetching all the tags
	app.get('/tags', function(req, res){
		service.get({req : req, res : res});
	});

};