var service = require('../services/properties-services');

module.exports = function(app){

	//Fetching all the properties
	app.get('/properties', function(req, res){
		service.getAll({req : req, res : res});
	});

};