var service = require('../services/employer-services'),
	auth = require('../services/auth'),
	multer  = require('multer'),
	uploadAvatar = multer({ dest: 'uploads/employers-avatars/' })
	globalService = require('../services/global-services'),
	matchService = require('../services/match-services');

module.exports = function(app){

	//Login
	app.get('/employer', auth.employerAuthenticated, function (req, res, next) {
		service.getEmployer({req : req, res : res}, req.query.email);
	});

	//Create a job
	app.post('/employer/jobs', auth.employerAuthenticated, function (req, res, next) {
		service.createJob({req : req, res : res}, req.body.email, req.body.job);
	});

	//Get employer jobs
	app.get('/employer/jobs', auth.employerAuthenticated, function (req, res, next) {
		service.getJobs({req : req, res : res}, req.query.email);
	});

	//Get employer matches
	app.get('/employer/matches', auth.employerAuthenticated, function (req, res, next) {
		matchService.getEmployerSeekers({req : req, res : res}, req.query.email);
	});

	//Register
	app.post('/employer', function(req, res){
    	service.register({req : req, res : res}, req.body.email, req.body.password);
	});	

	//Upload new avatar
	app.post('/employer/:email/picture', uploadAvatar.single('avatar'), function (req, res, next) {
		service.addPicture({req : req, res : res}, req.params.email, req.body.email, req.file);
	});

	//Update
	app.put('/employer/:email', auth.employerAuthenticated, function(req, res){
    	service.update({req : req, res : res}, req.params.email, req.body);
	});

	//Check mail availability
	app.get('/employer/mail/:email/available', function(req, res){
		service.employerMailAvailable({req : req, res : res}, req.params.email);
	});

	//Delete an employer
	app.delete('/employer/:email', auth.employerAuthenticated, function(req, res){
		service.delete({req : req, res : res}, req.params.email);
	});

	//Get a Job
	app.get('/employer/jobs/:id', auth.employerAuthenticated, function(req, res, next) {
		globalService.getJobById({req : req, res : res}, req.params.id);
	});

	//Sort jobs
	app.put('/employer/jobs/sort', auth.employerAuthenticated, function(req, res){
    	service.sortJobs({req : req, res : res}, req.body.email, req.body.array);
	});

	//Update a Job
	app.put('/employer/jobs/:id', auth.employerAuthenticated, function(req, res, next) {
		service.updateJob({req : req, res : res}, req.params.id, req.body);
	});

	//Get seekers for job
	app.get('/employer/jobs/:id/seekers', auth.employerAuthenticated, function(req, res, next) {
		matchService.getSeekersForJob({req : req, res : res}, req.params.id, req.query.email);
	});

	//Discard a seeker
	app.post('/employer/jobs/:id/discard', function(req, res){
    	service.discard({req : req, res : res}, req.params.id, req.body.seekerId);
	});	

	//Favor a seeker
	app.post('/employer/jobs/:id/favor', function(req, res){
    	service.favor({req : req, res : res}, req.params.id, req.body.seekerId);
	});	

	//Undiscard a seeker
	app.post('/employer/jobs/:id/undiscard', function(req, res){
    	service.undiscard({req : req, res : res}, req.params.id, req.body.seekerId);
	});	

	//Unfavor a seeker
	app.post('/employer/jobs/:id/unfavor', function(req, res){
    	service.unfavor({req : req, res : res}, req.params.id, req.body.seekerId);
	});

	//Get seeker details
	app.get('/employer/seeker/:id', function(req, res, next) {
		service.getSeeker({req : req, res : res}, req.params.id);
	});
};