var service = require('../services/seeker-services'),
	matchService = require('../services/match-services'),
	auth = require('../services/auth'),
	multer  = require('multer'),
	uploadCv = multer({ dest: 'uploads/seeker-cvs/' });

module.exports = function(app){

	//Login
	app.get('/seeker', auth.seekerAuthenticated, function (req, res, next) {
		service.getSeeker({req : req, res : res}, req.query.email);
	});

	//Register
	app.post('/seeker', function(req, res){
    	service.register({req : req, res : res}, req.body.email, req.body.password, req.body.name);
	});

	//Check mail availability
	app.get('/seeker/mail/:mail/available', function(req, res){
		service.seekerMailAvailable({req : req, res : res}, req.params.mail);
	});

	//Update
	app.put('/seeker/:email', auth.seekerAuthenticated, function(req, res){
    	service.update({req : req, res : res}, req.params.email, req.body);
	});

	//Delete a seeker
	app.delete('/seeker/:email', auth.seekerAuthenticated, function(req, res){
		service.delete({req : req, res : res}, req.params.email);
	});

	//Upload new cv
	app.post('/seeker/:email/cv', uploadCv.single('cv'), function (req, res, next) {
		service.addCv({req : req, res : res}, req.params.email, req.body.email, req.file);
	});

	//Get matching jobs
	app.get('/seeker/:mail/jobs', function(req, res){
		matchService.getJobForSeekers({req : req, res : res}, req.params.mail);
	});

	//Discard a job
	app.post('/seeker/jobs/:id/discard', function(req, res){
    	service.discard({req : req, res : res}, req.params.id, req.body.email);
	});	

	//Favor a job
	app.post('/seeker/jobs/:id/favor', function(req, res){
    	service.favor({req : req, res : res}, req.params.id, req.body.email);
	});	

	//Undiscard a job
	app.post('/seeker/jobs/:id/undiscard', function(req, res){
    	service.undiscard({req : req, res : res}, req.params.id, req.body.email);
	});	

	//Unfavor a job
	app.post('/seeker/jobs/:id/unfavor', function(req, res){
    	service.unfavor({req : req, res : res}, req.params.id, req.body.email);
	});

	//Get a Job
	app.get('/employer/jobs/:id', function(req, res, next) {
		globalService.getJobById({req : req, res : res}, req.params.id);
	});

	//Get employer details
	app.get('/seeker/employer/:id', function(req, res, next) {
		service.getEmployer({req : req, res : res}, req.params.id);
	});
};