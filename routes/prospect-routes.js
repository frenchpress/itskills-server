var service = require('../services/prospect-services');

module.exports = function(app){
	
	//Login
	app.post('/prospect/employer', function (req, res, next) {
		service.registerProspectEmp({req : req, res : res}, req.body.email);
	});

	//Register
	app.post('/prospect/seeker', function(req, res){
    	service.registerProspectSeeker({req : req, res : res}, req.body.email);
	});
};