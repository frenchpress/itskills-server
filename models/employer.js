var mongoose = require('mongoose'),
	bcrypt = require('bcrypt-nodejs');

var EmployerSchema = mongoose.Schema({
    email: String,
    password : String,
    companyName: String,
    address : String,
    phone : String,
    website : String,
    size : Number,
    industry : Number,
    description : String,
    avatar : String,
    social : [{
        id : Number,
        url : String
    }],
    plan : Number,
    planDate : Date,
    active : Boolean
});

EmployerSchema.methods.verifyPassword = function(password, callback) {
	bcrypt.compare(password, this.password, function(err, isMatch) {
		if (err){
			return callback(err);
		}
		callback(null, isMatch);
	});
};

var Employer;

if (mongoose.models.Employer) {
  Employer = mongoose.model('Employer');
} else {
  Employer = mongoose.model('Employer', EmployerSchema);
}

module.exports = Employer;