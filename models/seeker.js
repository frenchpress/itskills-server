var mongoose = require('mongoose');
	bcrypt = require('bcrypt-nodejs');

var SeekerSchema = mongoose.Schema({
    email: String,
    password: String,
    name: String,
    location: Number,
    jobTitle: String,
    phone: String,
    xp: Number, 
    jobTypes: [Number],
    companySizes: [Number],
    education : Number,
    industries: [Number],
    status : Number,
    skills: [String],
    discarded : [String],
    favored : [String],
    salary: Number,
    cv: String,
    social: [{
        id : Number,
        url : String
    }],
    description : String,
    active : Boolean
});

SeekerSchema.methods.verifyPassword = function(password, callback) {
	bcrypt.compare(password, this.password, function(err, isMatch) {
		if (err){
			return callback(err);
		}
		callback(null, isMatch);
	});
};

var Seeker;

if (mongoose.models.Seeker) {
  Seeker = mongoose.model('Seeker');
} else {
  Seeker = mongoose.model('Seeker', SeekerSchema);
}

module.exports = Seeker;