var mongoose = require('mongoose');

var EmployerProspectSchema = mongoose.Schema({
    email: String,
    date : Date
});

var EmployerProspect;

if (mongoose.models.EmployerProspect) {
  EmployerProspect = mongoose.model('EmployerProspect');
} else {
  EmployerProspect = mongoose.model('EmployerProspect', EmployerProspectSchema);
}

module.exports = EmployerProspect;