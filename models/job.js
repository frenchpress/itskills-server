var mongoose = require('mongoose');

var JobSchema = mongoose.Schema({
    title : String,
	employer : String,
	tags : [String],
	description : String,
	location : Number,
	salaryMin : Number,
	salaryMax : Number,
	xp : [Number],
	types : [Number],
	discarded : [String],
	favored : [String],
	mail : String,
	views : Number,
	rank : Number,
	roles : String,
	whyUs : String,

	created : Date, 
	started : Number,
	active : Boolean,
	complete : Boolean,
	visible : Boolean
});

var Job;

if (mongoose.models.Job) {
  Job = mongoose.model('Job');
} else {
  Job = mongoose.model('Job', JobSchema);
}

module.exports = Job;