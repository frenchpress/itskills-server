var mongoose = require('mongoose');

var TagSchema = mongoose.Schema({
    label: String,
    count : String
});

var Tag;

if (mongoose.models.Tag) {
  Tag = mongoose.model('Tag');
} else {
  Tag = mongoose.model('Tag', TagSchema);
}

module.exports = Tag;