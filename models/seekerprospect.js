var mongoose = require('mongoose');

var SeekerProspectSchema = mongoose.Schema({
    email: String,
    date : Date
});

var SeekerProspect;

if (mongoose.models.SeekerProspect) {
  SeekerProspect = mongoose.model('SeekerProspect');
} else {
  SeekerProspect = mongoose.model('SeekerProspect', SeekerProspectSchema);
}

module.exports = SeekerProspect;